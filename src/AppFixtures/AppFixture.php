<?php

namespace App\AppFixtures;

use App\Factory\HotelFactory;
use App\Factory\ReservationFactory;
use App\Factory\RoomFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixture extends Fixture
{
    const USER_NBR = 10;
    const HOTEL_NBR = 10;
    const ROOM_NBR = 10;
    const RESERVATION_NBR = 10;

    public function load(ObjectManager $manager): void
    {
        UserFactory::createMany(self::USER_NBR);
        HotelFactory::createMany(self::HOTEL_NBR);
        RoomFactory::createMany(self::ROOM_NBR);
        ReservationFactory::createMany(self::RESERVATION_NBR);
    }
}
