<?php

namespace App\Entity;

use App\Repository\RoomRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoomRepository::class)]
class Room
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $roomNbr = null;

    #[ORM\Column]
    private ?int $capacity = null;

    #[ORM\Column]
    private ?int $priceNight = null;

    #[ORM\Column]
    private ?bool $avalibility = null;

    #[ORM\Column(length: 100)]
    private ?string $view = null;

    #[ORM\Column(length: 100)]
    private ?string $bedType = null;

    #[ORM\Column]
    private ?bool $smokingPref = null;

    #[ORM\ManyToOne(inversedBy: 'rooms')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Hotel $hotel = null;

    #[ORM\ManyToOne(inversedBy: 'room')]
    private ?Reservation $reservation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoomNbr(): ?int
    {
        return $this->roomNbr;
    }

    public function setRoomNbr(int $roomNbr): static
    {
        $this->roomNbr = $roomNbr;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): static
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getPriceNight(): ?int
    {
        return $this->priceNight;
    }

    public function setPriceNight(int $priceNight): static
    {
        $this->priceNight = $priceNight;

        return $this;
    }

    public function isAvalibility(): ?bool
    {
        return $this->avalibility;
    }

    public function setAvalibility(bool $avalibility): static
    {
        $this->avalibility = $avalibility;

        return $this;
    }

    public function getView(): ?string
    {
        return $this->view;
    }

    public function setView(string $view): static
    {
        $this->view = $view;

        return $this;
    }

    public function getBedType(): ?string
    {
        return $this->bedType;
    }

    public function setBedType(string $bedType): static
    {
        $this->bedType = $bedType;

        return $this;
    }

    public function isSmokingPref(): ?bool
    {
        return $this->smokingPref;
    }

    public function setSmokingPref(bool $smokingPref): static
    {
        $this->smokingPref = $smokingPref;

        return $this;
    }

    public function getHotel(): ?Hotel
    {
        return $this->hotel;
    }

    public function setHotel(?Hotel $hotel): static
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function setReservation(?Reservation $reservation): static
    {
        $this->reservation = $reservation;

        return $this;
    }
}
