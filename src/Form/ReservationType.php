<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('checkIn', DateTimeType::class, [
                'widget' => 'single_text',
                'label' => 'Check In'
            ])
            ->add('checkOut', DateTimeType::class, [
                'widget' => 'single_text',
                'label' => 'Check Out'
            ])
            ->add('specialRequests', TextType::class, [
                'label' => 'Special Requests',
                'required' => false
            ])
            ->add('guestNbr', IntegerType::class, [
                'label' => 'Number of Guests'
            ])
            ->add('promoCode', TextType::class, [
                'label' => 'Promo Code',
                'required' => false
            ])
            ->add('hotel', null, [
                'choice_label' => 'name',
                'label' => 'Hotel'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
