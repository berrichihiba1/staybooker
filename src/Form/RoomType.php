<?php

namespace App\Form;

use App\Entity\Hotel;
use App\Entity\Room;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('roomNbr', NumberType::class)
            ->add('capacity', NumberType::class)
            ->add('priceNight', NumberType::class)
            ->add('avalibility', CheckboxType::class, [
                'required' => false,
            ])
            ->add('view', TextType::class)
            ->add('bedType', TextType::class)
            ->add('smokingPref', CheckboxType::class, [
                'required' => false,
            ])
            ->add('hotel', EntityType::class, [
                'class' => Hotel::class,
                'choice_label' => 'name',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
        ]);
    }
}
