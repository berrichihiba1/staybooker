<?php

namespace App\Factory;

use App\Entity\Room;
use App\Repository\HotelRepository;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Room>
 */
final class RoomFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct(
        private readonly HotelRepository $hotelRepository,
    ){
    }

    public static function class(): string
    {
        return Room::class;
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function defaults(): array|callable
    {
        return [
            'avalibility' => self::faker()->boolean(),
            'bedType' => self::faker()->text(100),
            'capacity' => self::faker()->numberBetween(1,10),
            'hotel' => HotelFactory::new(),
            'priceNight' => self::faker()->numberBetween(100,1000),
            'roomNbr' => self::faker()->randomNumber(),
            'smokingPref' => self::faker()->boolean(),
            'view' => self::faker()->text(100),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this
            ->afterInstantiate(function(Room $room): void {
                $hotels = $this->hotelRepository->findAll();

                $room->setHotel(self::faker()->randomElement($hotels));
            })
        ;
    }
}
