<?php

namespace App\Factory;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use App\Repository\RoomRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Self_;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Reservation>
 */
final class ReservationFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly RoomRepository $roomRepository,
    ){
    }

    public static function class(): string
    {
        return Reservation::class;
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function defaults(): array|callable
    {
        return [
            'checkIn' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'checkOut' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'guestNbr' => self::faker()->randomNumber(),
            'hotel' => HotelFactory::new(),
            'paymentStatus' => self::faker()->text(100),
            'promoCode' => self::faker()->text(5),
            'specialRequests' => [self::faker()->randomElements(['connecting Rooms','Quiet zone','Pet Bed'])],
            'totalPrice' => self::faker()->numberBetween(60,500),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this
            ->afterInstantiate(function(Reservation $reservation): void {
                $users = $this->userRepository->findAll();
                $rooms = $this->roomRepository->findAll();

                $reservation->setClient(self::faker()->randomElement($users));
                $reservation->addRoom(self::faker()->randomElement($rooms));
            })
        ;
    }
}
