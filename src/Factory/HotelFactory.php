<?php

namespace App\Factory;

use App\Entity\Hotel;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<Hotel>
 */
final class HotelFactory extends PersistentProxyObjectFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
    }

    public static function class(): string
    {
        return Hotel::class;
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function defaults(): array|callable
    {
        return [
            'timeIn' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'timeOut' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'category' => self::faker()->text(100),
            'contact' => self::faker()->numberBetween(600000000,699999999),
            'description' => self::faker()->paragraph(),
            'location' => self::faker()->address(),
            'name' => self::faker()->name(100),
            'policies' => self::faker()->paragraph(),
            'rating' => self::faker()->numberBetween(0,5),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): static
    {
        return $this
            ->afterInstantiate(function(Hotel $hotel): void {
                $hotel->setWebsite($hotel->getName() . self::faker()->domainName() );
            })
        ;
    }
}
