<?php

namespace App\Controller;

use App\Entity\Hotel;
use App\Entity\Room;
use App\Form\HotelType;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HotelController extends AbstractController
{
    #[Route('/hotel', name: 'app_hotel', priority: 1)]
    public function index(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $queryBuilder = $entityManager->getRepository(Room::class)->createQueryBuilder('r');
        $queryBuilder->select('r')
            ->orderBy('r.priceNight', 'DESC')
            ->setMaxResults(3);

        $luxuryRooms = $queryBuilder->getQuery()->getResult();

        $queryBuilder = $entityManager->getRepository(Hotel::class)->createQueryBuilder('h');
        $queryBuilder->select('h.category')
            ->orderBy('h.category', 'DESC')
            ->distinct(true);

        $categories = $queryBuilder->getQuery()->getResult();

        // Add slug to each category
        $slugify = new Slugify();
        foreach ($categories as &$category) {
            $category['slug'] = $slugify->slugify($category['category']);
        }

        $hotels = $entityManager->getRepository(Hotel::class)->findAll();

        $pagination = $paginator->paginate(
            $hotels,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('hotel/index.html.twig', [
            'pagination' => $pagination,
            'luxuryRooms' => $luxuryRooms,
            'categories' => $categories,
        ]);
    }

    #[Route('/hotel/add', name: 'app_hotel_add')]
    public function add(EntityManagerInterface $entityManager, Request $request): Response
    {
        $randomHotel = $entityManager->getRepository(Hotel::class)->findOneBy([], ['id' => 'ASC']);

        $hotel = new Hotel();
        $form = $this->createForm(HotelType::class, $hotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $photoFile = $form->get('photo')->getData();

            if ($photoFile) {
                $newFilename = uniqid().'.'.$photoFile->guessExtension();

                $photoFile->move(
                    $this->getParameter('photos_directory'),
                    $newFilename
                );

                $hotel->setPhoto($newFilename);
            }

            $entityManager->persist($hotel);
            $entityManager->flush();

            return $this->redirectToRoute('app_hotel');
        }

        return $this->render('hotel/add.html.twig', [
            'form' => $form,
            'randomHotel' => $randomHotel,
        ]);
    }

    #[Route('/hotel/edit/{id}', name: 'app_hotel_edit')]
    public function edit(int $id, EntityManagerInterface $entityManager, Request $request): Response
    {
        $hotel = $entityManager->getRepository(Hotel::class)->find($id);

        if (!$hotel) {
            throw $this->createNotFoundException('The hotel does not exist');
        }

        $form = $this->createForm(HotelType::class, $hotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $photoFile = $form->get('photo')->getData();

            if ($photoFile) {
                $newFilename = uniqid().'.'.$photoFile->guessExtension();

                $photoFile->move(
                    $this->getParameter('photos_directory'),
                    $newFilename
                );

                $hotel->setPhoto($newFilename);
            }

            $entityManager->flush();

            return $this->redirectToRoute('app_hotel');
        }

        return $this->render('hotel/edit.html.twig', [
            'form' => $form,
            'hotel' => $hotel,
        ]);
    }

    #[Route('/hotel/{id}', name: 'app_hotel_rooms')]
    public function hotelRooms(): Response
    {
        return $this->render('hotel/rooms.html.twig');
    }

    #[Route('/hotel/delete/{id}', name: 'app_hotel_delete')]
    public function delete(int $id, EntityManagerInterface $entityManager): Response
    {
        $hotel = $entityManager->getRepository(Hotel::class)->find($id);

        if ($hotel) {
            $entityManager->remove($hotel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_hotel');
    }
}
