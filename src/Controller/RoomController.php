<?php

namespace App\Controller;

use App\Entity\Room;
use App\Form\RoomType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class RoomController extends AbstractController
{
    #[Route('/room', name: 'app_room')]
    public function index(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $rooms = $entityManager->getRepository(Room::class)->findAll();

        $pagination = $paginator->paginate(
            $rooms,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('room/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route('/room/add', name: 'app_room_add')]
    public function add(EntityManagerInterface $entityManager, Request $request): Response
    {
        $randomRoom = $entityManager->getRepository(Room::class)->findOneBy(['avalibility' => true], ['id' => 'ASC']);

        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($room);
            $entityManager->flush();
            return $this->redirectToRoute('app_room');
        }

        return $this->render('room/add.html.twig', [
            'form' => $form,
            'randomRoom' => $randomRoom,
        ]);
    }

    #[Route('/room/edit/{id}', name: 'app_room_edit')]
    public function edit(int $id, EntityManagerInterface $entityManager, Request $request): Response
    {
        $room = $entityManager->getRepository(Room::class)->find($id);
        if (!$room) {
            throw $this->createNotFoundException('The room does not exist');
        }

        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('app_room');
        }

        return $this->render('room/edit.html.twig', [
            'form' => $form,
            'room' => $room,
        ]);
    }

    #[Route('/room/delete/{id}', name: 'app_room_delete')]
    public function delete(Room $room, EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $rooms = $entityManager->getRepository(Room::class)->findAll();

        $pagination = $paginator->paginate(
            $rooms,
            $request->query->getInt('page', 1),
            6
        );

        $entityManager->remove($room);
        $entityManager->flush();

        return $this->redirectToRoute('app_room');
    }
}
