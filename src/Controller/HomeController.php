<?php

namespace App\Controller;

use App\Entity\Hotel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_USER')]
class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $hotels = $entityManager
            ->createQuery('SELECT h FROM App\Entity\Hotel h')
            ->setMaxResults(3)
            ->getResult();

        return $this->render('home/index.html.twig', [
            'hotels' => $hotels,
        ]);
    }
}
