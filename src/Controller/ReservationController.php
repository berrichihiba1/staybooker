<?php

namespace App\Controller;

use App\Entity\Hotel;
use App\Entity\Reservation;
use App\Form\ReservationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReservationController extends AbstractController
{
    #[Route('/reservations', name: 'app_reservation')]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();

        $reservations = $entityManager->getRepository(Reservation::class)->findBy(['client' => $user]);

        return $this->render('reservation/index.html.twig', [
            'reservations' => $reservations,
        ]);
    }

    #[Route('/reservations/new', name: 'app_reservation_new')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $reservation = new Reservation();
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reservation->setClient($this->getUser());
            $entityManager->persist($reservation);
            $entityManager->flush();
            return $this->redirectToRoute('app_reservation');
        }

        $randomHotel = $entityManager->getRepository(Hotel::class)->findOneBy([], ['id' => 'ASC']);

        return $this->render('reservation/new.html.twig', [
            'form' => $form->createView(),
            'randomHotel' => $randomHotel,
        ]);
    }
}
