<?php

namespace App\Controller;

use App\Entity\Hotel;
use App\Entity\Room;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    #[Route('/category/{slug}', name: 'app_category')]
    public function index(string $slug, EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $queryBuilder = $entityManager->getRepository(Room::class)->createQueryBuilder('r');
        $queryBuilder->select('r')
            ->orderBy('r.priceNight', 'DESC')
            ->setMaxResults(3);

        $luxuryRooms = $queryBuilder->getQuery()->getResult();

        $queryBuilder = $entityManager->getRepository(Hotel::class)->createQueryBuilder('h');
        $queryBuilder->select('h.category')
            ->orderBy('h.category', 'DESC')
            ->distinct(true);

        $categories = $queryBuilder->getQuery()->getResult();

        $slugify = new Slugify();
        foreach ($categories as &$category) {
            $category['slug'] = $slugify->slugify($category['category']);
        }

        $category = $slugify->slugify($slug);

        $hotels = $entityManager->getRepository(Hotel::class)->findBy(['category' => $category]);

        $pagination = $paginator->paginate(
            $hotels,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('category/index.html.twig', [
            'pagination' => $pagination,
            'luxuryRooms' => $luxuryRooms,
            'categories' => $categories,
        ]);
    }
}
